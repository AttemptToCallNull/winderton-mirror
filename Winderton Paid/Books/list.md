# Update #1

### Повсыедневное Чтение

* Гёдель, Эшер, Бах
* The Healthy Programmer.

### C

* Язык программирования Си
* Learn C the hard way
* The C Programming Language

### C++

* Stroustrup: Programming -- Principles and Practice Using C++
* Thinking in C++

### Computer Science

* The Mythical Man-Month
* The Pragmatic Programmer: your journey to mastery, 20th Anniversary Edition 
* Alan Turing Essey
* Assembly language for x86 processors
* Computer Science an overview

#### Advanced

* The Art of Computer Programming
* Types and Programming Languages
* SICP
* The elements of computing systems

#### Algorithms and Data Sctuctures

* Introduction to Algorithms, Third Edition

### Databases

* Seven Databases in Seven Weeks

### English

* English Grammar in Use Blue
* English Grammar in Use Green

### Game Programming

* Game Development with Swift
* Game Programming Patterns
* Game Engine Architecture by Jason Gregory
* Game Coding Complete
* Multiplayer Game Programming: Architecting Networked Games

### Go

* The Go programming language

### Graphics

* Beginning DirectX 11 Game Programming - Allen Sherrod, Wendy Jones

### Hacking

* Hacking: The Art of Exploitation

### Java

* Effective Java
* Java a beginners guide
* Java Network Programming
* TCP/IP Sockets in Java Bundle

### Linux

* The Linux Command Line by William Shotts

### Math

* Справочник по элементарной математике - Выгодский
* Arithmetic and Algebra
* Linear Algebra Step by Step

### Networking

* Computer Networking: A Top-Down Approach

#### Unix Sockets

* Unix Network Programming, Volume 1: The Sockets Networking API

### Operating system

* Operating System Principles
* Operating Systems Three Easy Pieces
* Operating Systems Principles and Practise

### Software Engineering

* Beginning Software Engineering - Rod Stephens

### Web (old)

* Let's Build Instagram with Ruby on Rails
* Programming NOTES (то-ли личные заметки, то-ли что, идентифицировать не удалось)

### Win32

* Windows System Programming

# Update #2

### Compilers

* Aho - Compilers

### Computer Systems Design

* Strategy and Product Development for Complex Systems

### OS development

* Writing a Simple Operating System — from Scratch

# Update #3

### Deep Learning

* Deep Learning - Ian Goodfellow

### English

* English Grammar in Use Red

### Software Engineering

* Soft Skills: The software developer's life manual
* The Self-Taught Programmer: The Definitive Guide to Programming Professionally
* Code Complete by Steve McConnell

# Update #4

* Meyers S. - Effective Modern C++
* Effective C++ 3rd Edition
* Calling conventions for different C++ compilers and operating systems

# Update #5

* Introduction to Computing Systems

# Update #6

* Accelerated C++ Practical Programming by Example
* Structured Computer Organization
* C99 Standart
* C++ Concurrency In Practice
* C++ Standards Drafts
* The Design and Implementation of the FreeBSD Operating System

# Update #7

* OpenGL 3.3 (Core Profile)

# Update #8

* AI For Game Developers
* Intel manual for x64
* Programming Game AI by Example

# Update #9

* Game Engine Architecture
* Modern X86 Assembly Language Programming
* The Design of the UNIX Operating System

# Update #10

* Distributed systems
* C++17 in Detail
* Concepts of Programming Languages

# Update #11

* Data Compression Explained
* A Malloc Tutorial - Marwan Burelle

# Update #12

* Modern C.pdf

# Update #13

* Concrete Mathematics
* Modern Compiler Design
* Practical File System Design
* The Design of Everyday Things