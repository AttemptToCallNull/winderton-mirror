#### Информация

**QIWI:** +79605041776

**SBER:** 5254770222307654

**PayPal:** recursi0n.edu@gmail.com

**Email:** winterforkato@gmail.com

---

**Водительские права, публичко показанные в Instagram**:

![Водительские права](./Info/_Proofs/drive-license.png)

***Законность публикации таких данных подтверждена следующим документом:*** [doc](https://drive.google.com/file/d/0B-tjyouebMp-QW9TeGRnaUdzeWM/view)

|  Profile       |                                        Original                                       | Archive | Archiving Date |
|:--------------:|:--------------------------------------------------------------------------------------|:--------|:--------------:|
| Toster         | [Yonghwa](https://toster.ru/user/Yonghwa/)                                            | `snapshot` [@Yonghwa](./Info/toster.ru%20%40Yonghwa/user) | 11.08.19 |
| Toster Q       | [Yonghwa/questions](https://toster.ru/user/Yonghwa/questions)                         | `snapshot` [@Yonghwa/questions](./Info/toster.ru%20%40Yonghwa/q/Yonghwa)                                                                                                                                                        | 11.08.19 |
| Mail.ru        | [Александр Пушков](https://otvet.mail.ru/profile/id92250379/)                         | `snapshot` [@id92250379](./Info/mail.ru%20%40id92250379/01_profile.png)                        | 14.08.2019 |
| Mail.ru Q      | [Александр Пушков/questions](https://otvet.mail.ru/profile/id92250379/questions/all/) | `snapshot` [@id92250379/questions/all/](./Info/mail.ru%20%40id92250379/)         | 14.08.2019 |
| Upwork         | [Roman Z.](https://www.upwork.com/o/profiles/users/_~0112cddb8487e1be94/)             | `screenshot` [@\_~0112cddb8487e1be94](./Info/upwork.com%20%40_~0112cddb8487e1be94/_~0112cddb8487e1be94.png)                                                                                                 | 12.08.2019 |
| Old VK         | [Roman Zimin](https://vk.com/id56675304)                                              | `snapshot` [@id56675304](./Info/vk.com%20%40id56675304/) | 12.08.2019 |
| Twitch         | [recursi0n](https://www.twitch.tv/recursi0n/following)                               | `screenshot` [@recursi0n](./Info/twitch.tv%20%40recursi0n/) | 12.08.2019 |
| **Modern**     | - | - | - |
| VK             | `archive.org snapshot` [Roman Zimin](https://web.archive.org/web/20190406093142/https://vk.com/wndtn) | `screenshot` [@wndtn](./Info/vk.com%20%40wndtn/)                                                                                                                                                      | 12.08.2019 |
|  Patreon       | [Winderton](https://www.patreon.com/join/winderton) | `screenshot` [@Winderton](./Info/patreon.com%20%40Winderton/Winderton.png)        | 12.08.2019 |
| Instagram      | [Winderton](https://www.instagram.com/winderton/)   | `photo` [@Winderton](./Info/instagram%20%40winderton/photo/view.md)               | 12.08.2019 |
|  Twitter       | [Windert0n](https://twitter.com/windert0n)          | `screenshot` [@windert0n](./Info/twitter.com%20%40windert0n/windert0n.png)        | 12.08.2019 |
| CyberForum     | [Zai](http://www.cyberforum.ru/members/612538.html) | `snapshot` [@612538](./Info/www.cyberforum.ru%20%40612538/)                       | 12.08.2019 |
|YouTube         | [Winderton](https://www.youtube.com/channel/UC4omkhNHsYLagT1o6hnmKQw) | `screenshot` `video` [@UC4omkhNHsYLagT1o6hnmKQw](./Info/youtube.com%20%40UC4omkhNHsYLagT1o6hnmKQw/view.md) | 14.08.2019 |
| Twitch         | [Winderton](https://www.twitch.tv/winderton) | [@winderton](./Info/twitch.tv%20%40winderton/view.md) | 14.08.2019 |
|Reddit          | [Winderton](https://www.reddit.com/user/winderton/) | [@winderton](./Info/www.reddit.com%20%40winderton) | 14.08.2019 |
|programmersforum| [Bachshwa](https://www.programmersforum.ru/search.php?searchid=2378301) | - | - |

* [![](https://sun9-56.userapi.com/c854220/v854220444/c4963/4RCIADAsHRY.jpg)](https://sun9-33.userapi.com/c855332/v855332179/ca02e/2EBNjHuetCk.jpg)
* [![](https://sun9-31.userapi.com/c854220/v854220444/c4972/_drLEX8hMFk.jpg)](https://pp.userapi.com/c855332/v855332179/c9fea/wV6rbng_6rk.jpg)
* [![](https://sun9-33.userapi.com/c854220/v854220444/c496a/UPqq0m8lPJM.jpg)](https://sun9-6.userapi.com/c855332/v855332179/ca012/FGBwI4GR2dY.jpg)
* [![](https://sun9-26.userapi.com/c854220/v854220444/c4996/URjFYUW9e3k.jpg)](https://sun9-1.userapi.com/c855332/v855332179/c9ffe/4JjXQtn7-Eg.jpg)
* [![](https://sun9-49.userapi.com/c854220/v854220444/c498d/yxlAjj9Uvmg.jpg)](https://pp.userapi.com/c855332/v855332179/ca008/0DIJYnawPmY.jpg)
* [![](https://pp.userapi.com/c854220/v854220444/c4984/1SgUmY7ksAE.jpg)](https://sun9-42.userapi.com/c855332/v855332179/c9ff4/5AZ825LMShg.jpg)

Книги:

* [DropBox](https://www.dropbox.com/sh/9uraqgy563q2qmv/AACZzzNMav6X2HKEw-42W5_fa?dl=0) (Книги под копирайтом заблокированны)
* [List](./Winderton%20Paid/Books)

---

#### Оригинальный слив:

* [ARM](https://yadi.sk/d/rqkYIMl24omt4Q)
* [Планы](https://trello.com/b/fNa6nNf1/oh-shit)
* [Java](https://trello.com/b/5TAvVmeq/java)
* [CS 2017](https://trello.com/b/7UtbZCnX/cs-%D0%B7%D0%B0-50-%D1%80%D1%83%D0%B1%D0%BB%D0%B5%D0%B9-25)
* [Пледжи](https://drive.google.com/drive/folders/1sZHJNtFV52RLmLZAMQ7N5w2ZFbUvoiJ7)
* [50 планов](https://trello.com/b/euHGhPVj/50-%D0%BF%D0%BB%D0%B0%D0%BD%D0%BE%D0%B2)

---

#### Видео:

1. [black sun](https://www.youtube.com/channel/UCUbIq1Jmo5xsMu0znI0sUyQ/videos)
2. [Winderton's Streams](https://www.youtube.com/channel/UCwW6-zZTFiKQUPsfJ_J9pbw/videos)
3. [winderton ГЕНИЙ IT](https://www.youtube.com/channel/UC81Dkl755QTH_zM6C9DEreQ/videos)

#### Треды:

[Thread 1 Original](./2ch%20threads/2ch.hk%20[1-thread]/fag/res/)

[Thread 1 Full (Archive)](./2ch%20threads/arhivach.ng%20[1-thread-full]/thread/472761/)

[Thread 2 (Saved: 11.08.2019)](./2ch%20threads/2ch.hk%20[2-thread]/fag/res/)


**Основываясь на данных из прав, а именно:** *ЗИМИН РОМАН ВЛАДИМИРОВИЧ 28.08.1992 ГОДА ИВАНОВСКАЯ ОБЛАСТЬ*, и по сайту [Федеральной службы судебных приставов](http://fssprus.ru/iss/ip), можно узнать следующие:

![debt](./Info/_Proofs/ФедеральнаяСлужбаСудебныхПриставов.png)
